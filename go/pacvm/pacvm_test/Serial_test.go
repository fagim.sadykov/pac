package pacvm_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/fagim.sadykov/pac/go/pacvm"
	"testing"
)

func TestSimpleU8SerialReader(t *testing.T) {
	sd := pacvm.BuildSerialDescriptor(func(builder *pacvm.SerialDescriptorBuilder) {
		builder.SetDirection(pacvm.IN)
	})
	data := []byte{1, 4, 5, 10}
	serial := sd.BuildSerialReaderB(data)
	for i := 0; i < 4; i++ {
		b, c, err := serial.Read(pacvm.BS8)
		assert.NotNil(t, b)
		assert.Nil(t, err)
		assert.Equal(t, 1, c)
		assert.Equal(t, data[i], b[0])
	}

	_, c, err := serial.Read(pacvm.BS8)
	assert.Equal(t, 0, c)
	assert.NotNil(t, err)
}

func TestSimpleU8SerialWriter(t *testing.T) {
	sd := pacvm.BuildSerialDescriptor(func(builder *pacvm.SerialDescriptorBuilder) {
		builder.SetDirection(pacvm.OUT)
	})
	serial := sd.BuildSerialWriter()
	for i := 0; i < 4; i++ {
		err := serial.Write([]byte{byte(i + 1)})
		assert.Nil(t, err)
	}
	b, err := serial.Bytes()
	assert.Nil(t, err)
	assert.Equal(t, []byte{1, 2, 3, 4}, b)
}
