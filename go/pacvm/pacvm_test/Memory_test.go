package pacvm_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/fagim.sadykov/pac/go/pacvm"
	"testing"
)

func TestMemory(t *testing.T) {
	t.Run("8 bit", func(t *testing.T) {
		doTestMemory(t, pacvm.BS8)
	})
	t.Run("16 bit", func(t *testing.T) {
		doTestMemory(t, pacvm.BS16)
	})
	t.Run("32 bit", func(t *testing.T) {
		doTestMemory(t, pacvm.BS32)
	})
	t.Run("64 bit", func(t *testing.T) {
		doTestMemory(t, pacvm.BS64)
	})
}

func doTestMemory(t *testing.T, size pacvm.UnitSize) {
	memDesc := pacvm.BuildMemoryDescriptor(func(builder *pacvm.MemoryDescriptorBuilder) {
		builder.SetUnitSize(size)
		builder.SetTotalSize(pacvm.M256b)
	})
	assert.Equal(t, uint8(size), memDesc.UnitSize())
	assert.Equal(t, uint64(256), memDesc.TotalSize())
	assert.Equal(t, uint64(256/uint64(size)*8), memDesc.CellCount())
	var max uint64 = 0
	switch size {
	case 8:
		max = pacvm.MaxU8
	case 16:
		max = pacvm.MaxU16
	case 32:
		max = pacvm.MaxU32
	case 64:
		max = pacvm.MaxU64
	}
	assert.Equal(t, uint64(max), memDesc.MaxValue())

	mem := memDesc.BuildMemory()
	assert.Equal(t, memDesc, mem.Descriptor())
	assert.Len(t, mem.Buffer(), int(memDesc.TotalSize()))

	err := mem.SetNum(5, max-10)
	assert.Nil(t, err)
	val, err := mem.GetNum(5)
	assert.Nil(t, err)
	assert.Equal(t, uint64(max-10), val)

	if size != pacvm.BS64 {
		err = mem.SetNum(5, max+10)
		assert.ErrorIs(t, err, &pacvm.MemoryUnitOverflow{})
	}

	err = mem.SetNum(mem.Descriptor().CellCount(), 30)
	assert.ErrorIs(t, err, &pacvm.MemoryIndexOverflow{})

	_, err = mem.GetNum(mem.Descriptor().CellCount())
	assert.ErrorIs(t, err, &pacvm.MemoryIndexOverflow{})
}
