package pacvm

import (
	"fmt"
	"regexp"
)

type RegisterDescriptor struct {
	name    string
	bitSize UnitSize
}

var registerNameRegex = regexp.MustCompile(`^[A-Z][A-Z\d]*$`)

func NewRegisterDescriptor(name string, bitSize UnitSize) *RegisterDescriptor {
	if bitSize != 8 && bitSize != 16 && bitSize != 32 && bitSize != 64 {
		panic("only 8,16,32,64 bit sizes supported")
	}
	if !registerNameRegex.MatchString(name) {
		panic(fmt.Errorf("register name `%s` no matches `%s`", name, registerNameRegex.String()))
	}
	return &RegisterDescriptor{name: name, bitSize: bitSize}
}

func (r *RegisterDescriptor) Name() string {
	return r.name
}

func (r *RegisterDescriptor) BitSize() uint8 {
	return uint8(r.bitSize)
}
