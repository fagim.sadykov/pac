package pacvm

import (
	"bytes"
	"io"
)

type InOutDirection uint8

const (
	IN  InOutDirection = 1
	OUT InOutDirection = 2
)

type SerialDescriptor struct {
	supportedUnitSizes []uint8
	supportMoveForward bool
	supportMoveBack    bool
	direction          InOutDirection
	disabled           bool
}

type Serial struct {
	descriptor *SerialDescriptor
	reader     io.Reader
	writer     io.Writer
	isBuffer   bool
}

func (s *Serial) Descriptor() *SerialDescriptor {
	return s.descriptor
}

type NotReader struct{}

func (e *NotReader) Error() string {
	return "NotReader"
}

type NotWriter struct{}

func (e *NotWriter) Error() string {
	return "NotWriter"
}

type NotBuffer struct{}

func (e *NotBuffer) Error() string {
	return "NotBuffer"
}

type NilReader struct{}

func (e *NilReader) Error() string {
	return "NilReader"
}

type NilWriter struct{}

func (e *NilWriter) Error() string {
	return "NilWriter"
}

type InvalidUnitSize struct{}

func (e *InvalidUnitSize) Error() string {
	return "InvalidUnitSize"
}

type ReadWaitLimitReached struct{}

func (e *ReadWaitLimitReached) Error() string {
	return "ReadWaitLimitReached"
}
func (s *Serial) Bytes() ([]byte, error) {
	if s.Descriptor().Direction() != OUT {
		return nil, &NotWriter{}
	}
	if !s.isBuffer {
		return nil, &NotBuffer{}
	}
	return s.writer.(*bytes.Buffer).Bytes(), nil
}
func (s *Serial) Write(b []byte) error {
	if s.Descriptor().Direction() != OUT {
		return &NotWriter{}
	}
	usize := UnitSize(len(b) * 8)
	if !s.Descriptor().IsUnitSizeSupported(usize) {
		return &InvalidUnitSize{}
	}
	if s.writer == nil {
		return &NilWriter{}
	}
	_, err := s.writer.Write(b)
	return err
}

func (s *Serial) Read(size UnitSize) ([]byte, int, error) {
	if s.Descriptor().Direction() != IN {
		return nil, 0, &NotReader{}
	}
	if s.reader == nil {
		return nil, 0, &NilReader{}
	}
	if !s.Descriptor().IsUnitSizeSupported(size) {
		return nil, 0, &InvalidUnitSize{}
	}
	res := make([]byte, uint8(size)/8)
	total := 0
	cnt, err := s.reader.Read(res)
	if err != nil {
		return res, 0, err
	}
	total += cnt
	retrys := 0
	for total < len(res) {
		retrys++
		if retrys >= 5 {
			return res, total, &ReadWaitLimitReached{}
		}
		cnt, err = s.reader.Read(res[total:])
		total += cnt
		if err != nil {
			return res, total, err
		}
	}
	return res, total, nil
}

func (sd *SerialDescriptor) BuildSerialReaderB(b []byte) *Serial {
	if sd.Direction() != IN {
		panic("cannot create serial reader for OUT descriptor")
	}
	return &Serial{
		descriptor: sd,
		reader:     bytes.NewReader(b),
	}
}

func (sd *SerialDescriptor) BuildSerialWriterW(writer io.Writer) *Serial {
	if sd.Direction() != OUT {
		panic("cannot create serial writer for IN descriptor")
	}
	return &Serial{
		descriptor: sd,
		writer:     writer,
	}
}

func (sd *SerialDescriptor) BuildSerialWriter() *Serial {
	if sd.Direction() != OUT {
		panic("cannot create serial writer for IN descriptor")
	}
	return &Serial{
		descriptor: sd,
		writer:     bytes.NewBuffer(nil),
		isBuffer:   true,
	}
}

type SerialDescriptorBuilder struct {
	target *SerialDescriptor
}

func (s *SerialDescriptor) Direction() InOutDirection {
	return s.direction
}

func (s *SerialDescriptor) SupportedUnitSizes() []uint8 {
	return s.supportedUnitSizes
}

func (s *SerialDescriptor) SupportMoveForward() bool {
	return s.supportMoveForward
}

func (s *SerialDescriptor) SupportMoveBack() bool {
	return s.supportMoveBack
}

func (s *SerialDescriptor) Disabled() bool {
	return s.disabled
}

func BuildSerialDescriptor(buildFunc func(builder *SerialDescriptorBuilder)) *SerialDescriptor {
	result := &SerialDescriptor{}
	builder := &SerialDescriptorBuilder{target: result}
	buildFunc(builder)
	if len(result.supportedUnitSizes) == 0 {
		result.supportedUnitSizes = append(result.supportedUnitSizes, uint8(BS8))
	}
	if result.direction == 0 {
		panic("direction not set")
	}
	return result
}

func (sb *SerialDescriptorBuilder) SetDirection(direction InOutDirection) {
	if sb.target.direction != 0 && sb.target.direction != direction {
		panic("direction already set to another value")
	}
	sb.target.direction = direction
}

func (sb *SerialDescriptorBuilder) Disable() {
	sb.target.disabled = true
}

func (sb *SerialDescriptorBuilder) AllowMoveForward() {
	sb.target.supportMoveForward = true
}

func (sb *SerialDescriptorBuilder) AllowMoveBack() {
	sb.target.supportMoveBack = true
}

func (sb *SerialDescriptorBuilder) AllowUnitSize(bitSize UnitSize) {
	for _, u := range sb.target.supportedUnitSizes {
		if u == uint8(bitSize) {
			return
		}
	}
	sb.target.supportedUnitSizes = append(sb.target.supportedUnitSizes, uint8(bitSize))
}

func (sd *SerialDescriptor) IsUnitSizeSupported(size UnitSize) bool {
	for _, u := range sd.supportedUnitSizes {
		if u == uint8(size) {
			return true
		}
	}
	return false
}
