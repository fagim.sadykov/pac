package pacvm

type UnitSize uint8

const (
	BS8  UnitSize = 8
	BS16 UnitSize = 16
	BS32 UnitSize = 32
	BS64 UnitSize = 64
)
