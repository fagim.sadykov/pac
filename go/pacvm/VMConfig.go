package pacvm

import "fmt"

type PacVmConfig struct {
	registers    map[string]*RegisterDescriptor
	serialInput  *SerialDescriptor
	serialOutput *SerialDescriptor
	memory       *MemoryDescriptor
}

type PacVmConfigBuilder struct {
	config *PacVmConfig
}

func BuildPacVmConfig(buildFunc func(builder *PacVmConfigBuilder)) *PacVmConfig {
	result := &PacVmConfig{
		registers: map[string]*RegisterDescriptor{},
	}
	builder := &PacVmConfigBuilder{config: result}
	buildFunc(builder)
	if len(result.registers) == 0 {
		panic("no registers added")
	}
	if result.serialInput == nil {
		result.serialInput = BuildSerialDescriptor(func(builder *SerialDescriptorBuilder) {
			builder.SetDirection(IN)
		})
	}
	if result.serialInput == nil {
		result.serialOutput = BuildSerialDescriptor(func(builder *SerialDescriptorBuilder) {
			builder.SetDirection(OUT)
		})
	}
	if result.memory == nil {
		result.memory = BuildMemoryDescriptor(func(builder *MemoryDescriptorBuilder) {
			builder.SetTotalSize(MMin)
			builder.SetUnitSize(BS8)
		})
	}
	return result
}

func (pb *PacVmConfigBuilder) AddRegister(name string, bitSize UnitSize) *RegisterDescriptor {
	register := NewRegisterDescriptor(name, bitSize)
	ex, ok := pb.config.registers[name]
	if ok {
		if ex.BitSize() != uint8(bitSize) {
			panic(fmt.Errorf("config already have `%s` (%v) register, trys to add %v", ex.name, ex.bitSize, bitSize))
		}
		return ex
	}
	pb.config.registers[name] = register
	return register
}

func (pb *PacVmConfigBuilder) SerialInput(buildFunc func(builder *SerialDescriptorBuilder)) *SerialDescriptor {
	if pb.config.serialInput != nil {
		panic("serial input already set")
	}
	pb.config.serialInput = BuildSerialDescriptor(func(builder *SerialDescriptorBuilder) {
		builder.SetDirection(IN)
		buildFunc(builder)
	})
	return pb.config.serialInput
}

func (pb *PacVmConfigBuilder) SerialOutput(buildFunc func(builder *SerialDescriptorBuilder)) *SerialDescriptor {
	if pb.config.serialOutput != nil {
		panic("serial input already set")
	}
	pb.config.serialOutput = BuildSerialDescriptor(func(builder *SerialDescriptorBuilder) {
		builder.SetDirection(OUT)
		buildFunc(builder)
	})
	return pb.config.serialOutput
}
