package pacvm

type PacVm struct {
	config *PacVmConfig
}

func NewPacVm(config *PacVmConfig) *PacVm {
	return &PacVm{
		config: config,
	}
}

func (V *PacVm) Config() *PacVmConfig {
	return V.config
}
