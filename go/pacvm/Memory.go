package pacvm

import (
	"encoding/binary"
)

type MemoryDescriptor struct {
	totalSize     uint64
	unitSize      uint8
	unitSizeBytes uint8
	maxValue      uint64
	cellCount     uint64
}

func (m *MemoryDescriptor) UnitSizeBytes() uint8 {
	return m.unitSizeBytes
}

func (m *MemoryDescriptor) TotalSize() uint64 {
	return m.totalSize
}

func (m *MemoryDescriptor) UnitSize() uint8 {
	return m.unitSize
}

func (m *MemoryDescriptor) CellCount() uint64 {
	return m.cellCount
}

func (m *MemoryDescriptor) MaxValue() uint64 {
	return m.maxValue
}

type Memory struct {
	descriptor *MemoryDescriptor
	buffer     []byte
}

func (m *Memory) Descriptor() *MemoryDescriptor {
	return m.descriptor
}

func (m *Memory) Buffer() []byte {
	return m.buffer
}

func (md *MemoryDescriptor) BuildMemory() *Memory {
	return &Memory{
		descriptor: md,
		buffer:     make([]byte, md.totalSize),
	}
}

func (m *Memory) GetNum(i uint64) (uint64, error) {
	bytes, err := m.GetUnitBytes(i)
	if err != nil {
		return 0, err
	}
	switch len(bytes) {
	case 1:
		return uint64(bytes[0]), nil
	case 2:
		return uint64(binary.LittleEndian.Uint16(bytes)), nil
	case 4:
		return uint64(binary.LittleEndian.Uint32(bytes)), nil
	default:
		return binary.LittleEndian.Uint64(bytes), nil
	}
}

func (m *Memory) GetUnitBytes(i uint64) ([]byte, error) {
	if i > m.descriptor.cellCount-1 {
		return nil, &MemoryIndexOverflow{}
	}
	addr := i * uint64(m.descriptor.unitSize) / 8
	return m.buffer[addr : addr+uint64(m.descriptor.unitSize)], nil
}

type MemoryUnitOverflow struct {
}

func (e *MemoryUnitOverflow) Error() string {
	return "MemoryUnitOverflow"
}

type MemoryIndexOverflow struct {
}

func (e *MemoryIndexOverflow) Error() string {
	return "MemoryIndexOverflow"
}

func (m *Memory) SetNum(i uint64, value uint64) error {
	if value > m.descriptor.MaxValue() {
		return &MemoryUnitOverflow{}
	}
	var bytes []byte
	switch m.descriptor.unitSize {
	case 8:
		bytes = []byte{uint8(value)}
	case 16:
		bytes = make([]byte, 2)
		binary.LittleEndian.PutUint16(bytes, uint16(value))
	case 32:
		bytes = make([]byte, 4)
		binary.LittleEndian.PutUint32(bytes, uint32(value))
	default:
		bytes = make([]byte, 8)
		binary.LittleEndian.PutUint64(bytes, value)
	}
	return m.SetUnitBytes(i, bytes)
}

func (m *Memory) SetUnitBytes(i uint64, bytes []byte) error {
	if i > m.descriptor.cellCount-1 {
		return &MemoryIndexOverflow{}
	}
	if len(bytes) != int(m.descriptor.UnitSizeBytes()) {
		return &MemoryUnitOverflow{}
	}
	addr := i * uint64(m.descriptor.unitSize) / 8
	copy(m.buffer[addr:addr+uint64(m.descriptor.unitSize)], bytes)
	return nil
}

type MemoryDescriptorBuilder struct {
	target *MemoryDescriptor
}

type MemSize uint64

const (
	MZero  MemSize = 0
	M256b  MemSize = 1 << 8
	M512b  MemSize = M256b << 1
	M1Kb   MemSize = M512b << 1
	M256Kb MemSize = M1Kb << 8
	M512Kb MemSize = M256Kb << 1
	M1Mb   MemSize = M512Kb << 1
	M2Mb   MemSize = M1Mb << 1
	M4Mb   MemSize = M2Mb << 1
	M8Mb   MemSize = M4Mb << 1
	MMin   MemSize = M256b
	MMax   MemSize = M8Mb

	MaxU8  = 0xFF
	MaxU16 = 0xFFFF
	MaxU32 = 0xFFFFFFFF
	MaxU64 = 0xFFFFFFFFFFFFFFFF
)

func BuildMemoryDescriptor(buildFunc func(builder *MemoryDescriptorBuilder)) *MemoryDescriptor {
	result := &MemoryDescriptor{}
	builder := &MemoryDescriptorBuilder{
		target: result,
	}
	buildFunc(builder)
	if result.totalSize == 0 {
		result.totalSize = uint64(MMin)
	}
	if result.unitSize == 0 {
		result.unitSize = uint8(BS8)
	}
	result.unitSizeBytes = result.unitSize / 8
	result.cellCount = result.totalSize / (uint64(result.unitSizeBytes))
	switch result.unitSize {
	case 8:
		result.maxValue = MaxU8
	case 16:
		result.maxValue = MaxU16
	case 32:
		result.maxValue = MaxU32
	default:
		result.maxValue = MaxU64
	}
	return result
}

func (b *MemoryDescriptorBuilder) SetTotalSize(ms MemSize) {
	if b.target.totalSize != 0 && b.target.totalSize != uint64(ms) {
		panic("total size already set")
	}
	b.target.totalSize = uint64(ms)
}

func (b *MemoryDescriptorBuilder) SetUnitSize(us UnitSize) {
	if b.target.unitSize != 0 && b.target.unitSize != uint8(us) {
		panic("total size already set")
	}
	b.target.unitSize = uint8(us)
}
